﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace IShopConnector {
	class CommandResult {
		private static HashSet<string> IGNORED_FIELDS = new HashSet<string>(new string[] {
			"Id", "CRDate", "CRUser", "LMDate", "LMUser", "DateCreated"});

		private List<string> fields = new List<string>();
		private List<object[]> rows = new List<object[]>();

		public string[] Fields {
			get {
				return fields.ToArray();
			}
		}

		public int Field(string name) {
			return fields.IndexOf(name);
		}

		public int Count {
			get {
				return rows.Count;
			}
		}

		public ICollection<object[]> Rows {
			get {
				return rows.AsReadOnly();
			}
		}

		public Dictionary<string, object> this[int index] {
			get {
				Dictionary<String, object> data = new Dictionary<string, object>();
				for(int i = 0; i < fields.Count(); i++) {
					object value = rows[index][i];
					value = Remap(fields[i], value);
					if(value != null)
						data.Add(fields[i], value);
				}
				return data;
			}
		}

		public List<Dictionary<string, object>> Data {
			get {
				List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
				for(int i = 0; i < Count; i++)
					items.Add(this[i]);
				return items;
			}
		}

		private CommandResult() {
		}

		public static CommandResult From(SqlConnection connection, String command) {
			CommandResult result = new CommandResult();

			SqlCommand cmd = new SqlCommand(command, connection);
			SqlDataReader reader = cmd.ExecuteReader();

			for(int i = 0; i < reader.FieldCount; i++) {
				string fieldName = reader.GetName(i);
				if(!IsForbidden(fieldName)) {
					result.fields.Add(fieldName);
				}
			}

			try {
				while(reader.Read()) {
					object[] row = new object[result.Fields.Length];
					for (int i = 0; i < row.Length; i++)
					{
						string name = "";

						try
						{
							name = reader[result.Fields[i]].GetType().Name;
						}
						catch { }

						if (name == "Byte[]")
							row[i] = Convert.ToBase64String((byte[])reader[result.Fields[i]]);
						else
							row[i] = reader[result.Fields[i]];
					}
					result.rows.Add(row);
				}
			} finally {
				reader.Close();
			}

			return result;
		}

		private object Remap(string name, object value) {
			if(value is DateTime) {
				value = ((DateTime)value).ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss");
			}

			return value;
		}

		private static bool IsForbidden(string name) {
			return IGNORED_FIELDS.Contains(name);
		}
	}
}
