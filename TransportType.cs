﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace IShopConnector {
	class TransportType {
		private List<TransportType> subTransports = new List<TransportType>();

		public TransportType(int id, string name, string apiName, string extname, string source, int idBeforeName, string keyName, string customQuery, TransportType[] subTransports) {
			this.Id = id;
			this.Name = name;
			this.APIName = apiName;
			this.ExternalName = extname;
			this.Source = source;
			this.IdBeforeName = idBeforeName;
			this.KeyName = keyName;
			this.CustomQuery = customQuery;
			this.subTransports.AddRange(subTransports);
		}

		public int Id {
			get;
			private set;
		}

		public string Name {
			get;
			private set;
		}

		public string ExternalName {
			get;
			private set;
		}

		public string Source {
			get;
			private set;
		}
	
		public int IdBeforeName {
			get;
			private set;
		}

		public string KeyName
		{
			get;
			private set;
		}

		public string CustomQuery
		{
			get;
			private set;
		}

		public string APIName
		{
			get;
			private set;
		}


		public ICollection<TransportType> SubTransports {
			get {
				return subTransports.AsReadOnly();
			}
		}

		public Dictionary<string, object> Load(SqlConnection connection, string externalId, string transportId, string customQuery) {

			CommandResult items;

			if(string.IsNullOrEmpty(customQuery))
				items = CommandResult.From(connection, "SELECT * FROM " + Source + " WHERE " + KeyName + " = '" + externalId + "' AND TransportId = '" + transportId + "'");
			else
				items = CommandResult.From(connection, customQuery);

			Dictionary<string, object> result = new Dictionary<string,object>();
			result.Add(Name, items.Data);
			foreach(TransportType tt in subTransports) {
				result.Add(tt.Name, tt.Load(connection, externalId, transportId, customQuery));
			}
			return result;
		}
	}
}
