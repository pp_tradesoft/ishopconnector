﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace IShopConnector {
	class ExecutionType {

		public ExecutionType(int id, string name)
		{
			this.Id = id;
			this.Name = name;
		}

		public int Id {
			get;
			private set;
		}

		public string Name {
			get;
			private set;
		}
	}
}
