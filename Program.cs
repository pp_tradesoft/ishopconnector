﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Serialization;
using System.Data;

namespace IShopConnector
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.IShopDatabase);
            connection.Open();

            try
            {

                Log("Loading transport types...");
                Dictionary<int, TransportType> transportTypes = LoadTransportTypes(connection, 0);
                Log("Loading execution types...");
                Dictionary<int, ExecutionType> executionTypes = LoadExecutionTypes(connection);

                CommandResult nextItems = CommandResult.From(connection, "SELECT * FROM TransportQueue WHERE Status = 1 ORDER BY TransportId ASC ");
                if (nextItems.Count == 0)
                    Log("Transport queue is empty.");
                else
                    Log("Transport queue length: " + nextItems.Count.ToString());
                for (int i = 0; i < nextItems.Count; i++)
                {
                    Dictionary<string, object> transport = nextItems[i];

                    Log("Assigning transport #" + transport["TransportId"] + ": ", Convert.ToInt32(transport["TransportId"]));
                    if (!ChangeStatus(connection, Convert.ToInt32(transport["TransportId"]), 1, 2))
                    {
                        Log("already in process", Convert.ToInt32(transport["TransportId"]));
                        continue;
                    }
                    else
                    {
                        Log("processing", Convert.ToInt32(transport["TransportId"]));
                    }

                    try
                    {
                        TransportType transportType = transportTypes[Convert.ToInt32(transport["TypeId"])];
                        ExecutionType executionType = executionTypes[Convert.ToInt32(transport["ExecutionTypeId"])];
                        int idBeforeName = transportType.IdBeforeName;
                        string requestData = "{}";

                        if (Convert.ToInt32(transport["TypeId"]) > 1)
                            if (!string.IsNullOrEmpty(transportType.CustomQuery))
                            {
                                SqlCommand cmd = new SqlCommand(transportType.CustomQuery, connection);
                                Log("CustomQuery: " + transportType.CustomQuery, Convert.ToInt32(transport["TransportId"]));
                                cmd.Parameters.Add(new SqlParameter("@TRANSPORTID", Convert.ToString(transport["TransportId"])));
                                cmd.Parameters.Add(new SqlParameter("@EXTERNALID", Convert.ToString(transport["ExternalId"])));
                                requestData = (string)cmd.ExecuteScalar();
                                Log("CustomQuery result: " + requestData, Convert.ToInt32(transport["TransportId"]));
                            }
                            else
                                requestData = new JavaScriptSerializer()
                                .Serialize(transportType.Load(connection, Convert.ToString(transport["ExternalId"]), Convert.ToString(transport["TransportId"]), transportType.CustomQuery));

                        if (Convert.ToInt32(transport["TypeId"]) == 7)
                        {
                            requestData = requestData.Replace("{\"outpost_warehouse_state\":", "{\"outpost_warehouse_states\": {\"outpost_warehouse_state\": [");
                            requestData = requestData.Substring(0, requestData.Length - 1) + "]}}";
                        }

                        Log("Time: " + Convert.ToString(System.DateTime.Now), Convert.ToInt32(transport["TransportId"]));
                        Log("URI: " + "/rest_api/shop_api/v1/" + transportType.APIName + "/" + (idBeforeName == 1 ? transport["ExternalId"] + "/" : "") + transportType.ExternalName + (idBeforeName == 0 & transportType.APIName != "outpost_warehouse" ? transport["ExternalId"] : ""), Convert.ToInt32(transport["TransportId"]));
                        Log("REQUEST: " + requestData, Convert.ToInt32(transport["TransportId"]));

                        string responseData = REST.Call("/rest_api/shop_api/v1/" + transportType.APIName + "/" + (idBeforeName == 1 ? transport["ExternalId"] + "/" : "") + transportType.ExternalName + (idBeforeName == 0 ? transport["ExternalId"] : ""), requestData, executionType.Id);
                        //Dictionary<string, object> response = (Dictionary<string, object>)new JavaScriptSerializer().DeserializeObject(responseData.);
                        Log("RESPONSE: " + responseData, Convert.ToInt32(transport["TransportId"]));

                        if (!responseData.Contains("\"success\":true"))
                            throw new Exception("request failed successfully");
                        else
                        {
                            Log("TransportType: " + transportType.Id.ToString() + " - " + transportType.Name.ToString(), Convert.ToInt32(transport["TransportId"]));

                            using (SqlConnection connection2 = new SqlConnection(Properties.Settings.Default.IShopDatabase))
                            {
                                connection2.Open();
                                using (SqlCommand query = new SqlCommand("dbo.InsertData", connection2))
                                {
                                    query.CommandType = CommandType.StoredProcedure;
                                    query.CommandTimeout = 600;

                                    query.Parameters.Add(new SqlParameter("json", responseData));
                                    query.Parameters.Add(new SqlParameter("transportType", transportType.Id.ToString()));
                                    query.Parameters.Add(new SqlParameter("executionType", executionType.Id.ToString()));
                                    query.Parameters.Add(new SqlParameter("updateFromResponse", Convert.ToInt32(transport["UpdateFromResponse"])));
                                    query.Parameters.Add(new SqlParameter("externalId", Convert.ToInt32(transport["ExternalId"])));
                                    query.Parameters.Add(new SqlParameter("transportId", Convert.ToInt32(transport["TransportId"])));

                                    var returnParameter = query.Parameters.Add("@ReturnVal", SqlDbType.Int);
                                    returnParameter.Direction = ParameterDirection.ReturnValue;

                                    query.ExecuteReader();

                                    var result = returnParameter.Value;
                                    //Log(result.ToString());
                                }
                                connection2.Close();
                            }
                        }

                        Log("Committing transport #" + transport["TransportId"] + ": ", Convert.ToInt32(transport["TransportId"]));
                        try
                        {
                            if (!ChangeStatus(connection, Convert.ToInt32(transport["TransportId"]), 2, 3))
                                throw new Exception("Cannot commit transport " + transport["TransportId"] + " in the database");

                            Log("done", Convert.ToInt32(transport["TransportId"]));
                        }
                        catch (Exception e)
                        {
                            Log("error (" + e.Message + ")", Convert.ToInt32(transport["TransportId"]));
                            throw e;
                        }
                    }
                    catch (Exception e)
                    {
                        Log("Transport #" + transport["TransportId"] + " error: " + e.Message + "\n" + e.StackTrace, Convert.ToInt32(transport["TransportId"]));
                        ChangeStatus(connection, Convert.ToInt32(transport["TransportId"]), 2, 1);

                    }
                }
            }
            finally
            {
                connection.Close();
            }
        }

        static Dictionary<int, TransportType> LoadTransportTypes(SqlConnection connection, int dependent)
        {
            Dictionary<int, TransportType> transports = new Dictionary<int, TransportType>();

            CommandResult result = CommandResult.From(connection, "SELECT * FROM TransportTypes where BaseTypeId " + (dependent == 0 ? "IS NULL" : ("= " + dependent)));
            for (int i = 0; i < result.Count; i++)
            {
                Dictionary<string, object> tdata = result[i];

                Dictionary<int, TransportType> subTransports = LoadTransportTypes(connection, Convert.ToInt32(tdata["TypeId"]));
                TransportType transport = new TransportType(
                    Convert.ToInt32(tdata["TypeId"]), Convert.ToString(tdata["Name"]), Convert.ToString(tdata["APIName"]), Convert.ToString(tdata["ExternalName"]), Convert.ToString(tdata["ObjectName"]), Convert.ToInt32(tdata["IdBeforeName"]), Convert.ToString(tdata["KeyName"]), Convert.ToString(tdata["CustomQuery"]),
                    subTransports.Values.ToArray());

                transports.Add(transport.Id, transport);
            }

            return transports;
        }

        static Dictionary<int, ExecutionType> LoadExecutionTypes(SqlConnection connection)
        {
            Dictionary<int, ExecutionType> executions = new Dictionary<int, ExecutionType>();

            CommandResult result = CommandResult.From(connection, "SELECT * FROM TransportExecutionTypes");
            for (int i = 0; i < result.Count; i++)
            {
                Dictionary<string, object> tdata = result[i];

                ExecutionType execution = new ExecutionType(Convert.ToInt32(tdata["ExecutionTypeId"]), Convert.ToString(tdata["Code"]));

                executions.Add(execution.Id, execution);
            }

            return executions;
        }

        static bool ChangeStatus(SqlConnection connection, int queueItemId, int fromStatus, int toStatus)
        {
            using (SqlTransaction tx = connection.BeginTransaction(System.Data.IsolationLevel.Serializable))
                try
                {
                    SqlCommand cmd = new SqlCommand("UPDATE TransportQueue SET Status=@TOSTATUS WHERE TransportId=@ID AND Status=@FROMSTATUS", connection);
                    cmd.Parameters.Add(new SqlParameter("@FROMSTATUS", fromStatus));
                    cmd.Parameters.Add(new SqlParameter("@TOSTATUS", toStatus));
                    cmd.Parameters.Add(new SqlParameter("@ID", queueItemId));
                    cmd.Transaction = tx;
                    cmd.ExecuteNonQuery();
                    tx.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Log(" error: " + e.Message + "\n" + e.StackTrace);
                    tx.Rollback();
                    return false;
                }
        }

        static bool ChangeStatus(SqlConnection connection, int queueItemId, int fromStatus, int toStatus, string message, int transaction)
        {
            using (SqlTransaction tx = connection.BeginTransaction(System.Data.IsolationLevel.Serializable))
                try
                {
                    SqlCommand cmd = new SqlCommand("UPDATE TransportQueue SET Status=@TOSTATUS, Message=@MESSAGE, TransactionId=@TRANSACTION WHERE TransportId=@ID AND Status=@FROMSTATUS", connection);
                    cmd.Parameters.Add(new SqlParameter("@FROMSTATUS", fromStatus));
                    cmd.Parameters.Add(new SqlParameter("@TOSTATUS", toStatus));
                    cmd.Parameters.Add(new SqlParameter("@ID", queueItemId));
                    cmd.Parameters.Add(new SqlParameter("@MESSAGE", message));
                    cmd.Parameters.Add(new SqlParameter("@TRANSACTION", transaction));
                    cmd.Transaction = tx;
                    cmd.ExecuteNonQuery();
                    tx.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Log(" error: " + e.Message + "\n" + e.StackTrace);
                    tx.Rollback();
                    return false;
                }
        }

        static void Log(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
            Console.WriteLine(message);
        }

        static void Log(string message, int transportId)
        {
            System.Diagnostics.Debug.WriteLine(message);
            Console.WriteLine(message);
            using (SqlConnection logConnection = new SqlConnection(Properties.Settings.Default.IShopDatabase))
                try
                {
                    logConnection.Open();
                    SqlCommand logCmd = new SqlCommand("INSERT INTO [Log] (TransportId, Message, DateCreated) VALUES (" + transportId + ",'" + message + "',getdate())", logConnection);
                    logCmd.ExecuteNonQuery();
                }
                catch (Exception e) { Console.WriteLine(e.Message); }
                finally { logConnection.Close(); }

        }

    }
}
