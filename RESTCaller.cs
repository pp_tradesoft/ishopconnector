﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace IShopConnector
{
    class REST
    {
        public static string Call(string uri, string data, int executionTypeId)
        {

            if (data.Contains("{\"\":["))
            {
                data = data.Substring(5);
                data = data.Substring(0, data.Length - 2);
            }


            Log("executionTypeId: " + executionTypeId);

            if (executionTypeId == 2 && (data.Contains("\"order\":") || data.Contains("\"order_item\":"))) {
                data = data.Replace("[{", "{");
                data = data.Replace("]}", "}");
            }
            

            byte[] dataBytes = Encoding.UTF8.GetBytes(data);
            string timeInSecondsSince1970 = ((int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds).ToString();
            string nonceorg = Convert.ToBase64String(Encoding.UTF8.GetBytes(((int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds).ToString()));
            //string nonce = nonceorg.Substring(0, 14);
            string nonce = Guid.NewGuid().ToString().Replace('-', 'a').Substring(0, 14);
            nonce = EscapeUriDataStringRfc3986(nonce);
            //nonce = "AlS1sDlCAuo";
            //timeInSecondsSince1970 = "1537858658";
            string url = Properties.Settings.Default.Hostname + uri;

            Log("URL: " + url);

            string tokenValue = Properties.Settings.Default.TokenValue;
            string consumerKey = Properties.Settings.Default.ConsumerKey;
            string consumerSecret = Properties.Settings.Default.ConsumerSecret;
            string tokenSecret = Properties.Settings.Default.TokenSecret;

            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            if (executionTypeId == 1)
                request.Method = "POST";
            else if (executionTypeId == 2)
                request.Method = "PUT";
            else if (executionTypeId == 3)
                request.Method = "GET";

            string signature_base_string = GetSignatureBaseString(timeInSecondsSince1970, nonce, url, request.Method);
            string SHA1HASH = EscapeUriDataStringRfc3986(GetSha1Hash(consumerSecret + "&" + tokenSecret, signature_base_string));

            string Header =
                "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("restuser:HSG-72shsSG"));

            /*
            //"OAuth realm=" + '"' + '"' + "," +
           "OAuth " +
           "oauth_consumer_key=" + '"' + consumerKey + '"' + "," +
           "oauth_token=" + '"' + tokenValue + '"' + "," +
           "oauth_signature_method=" + '"' + "HMAC-SHA1" + '"' + "," +
           "oauth_timestamp=" + '"' + timeInSecondsSince1970 + '"' + "," +
           "oauth_nonce=" + '"' + nonce + '"' + "," +
           "oauth_version=" + '"' + "1.0" + '"' + "," +
           "oauth_signature=" + '"' + SHA1HASH + '"'; 
            */

            request.Headers.Add(HttpRequestHeader.Authorization, Header);
            request.Timeout = 180000;
            request.ContentType = "application/json";

            Log("Headers: ");
            Log(request.Headers.ToString());
            Log("Method: " + request.Method.ToString());

            if (executionTypeId != 3)
            {
                var str = System.Text.Encoding.Default.GetString(dataBytes);

                Log("Request body: ");
                Log(str);

                request.ContentLength = dataBytes.Length;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(dataBytes, 0, dataBytes.Length);
                }
            }

            //return "TEST";

            try
            {
                WebResponse response = request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                        return "";

                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                {
                    if (stream == null)
                        return "";

                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }

            };

        }

        private static bool CertificateValidationCallBack(
         object sender,
         System.Security.Cryptography.X509Certificates.X509Certificate certificate,
         System.Security.Cryptography.X509Certificates.X509Chain chain,
         System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static long GetUnixTimestamp(DateTime date)
        {
            long unixTimestamp = date.Ticks - new DateTime(1970, 1, 1).Ticks;
            unixTimestamp /= TimeSpan.TicksPerSecond;
            return unixTimestamp;
        }

        public static int GetUtcTimestamp()
        {
            return (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static string GetSha1Hash(string key, string message)
        {
            //var encoding = new System.Text.ASCIIEncoding();
            var encoding = new System.Text.UTF8Encoding();

            byte[] keyBytes = encoding.GetBytes(key);
            byte[] messageBytes = encoding.GetBytes(message);

            string Sha1Result = string.Empty;

            using (HMACSHA1 SHA1 = new HMACSHA1(keyBytes))
            {
                var Hashed = SHA1.ComputeHash(messageBytes);
                Sha1Result = Convert.ToBase64String(Hashed);
            }

            return Sha1Result;
        }

        public static string GetSignatureBaseString(string timeStamp, string nonce, string url, string method)
        {
            //1.Convert the HTTP Method to uppercase and set the output string equal to this value.
            string Signature_Base_String = method;
            Signature_Base_String = Signature_Base_String.ToUpper();

            //2.Append the ‘&’ character to the output string.
            Signature_Base_String = Signature_Base_String + "&";

            //3.Percent encode the URL and append it to the output string.
            string PercentEncodedURL = Uri.EscapeDataString(url);
            Signature_Base_String = Signature_Base_String + PercentEncodedURL;

            //4.Append the ‘&’ character to the output string.
            Signature_Base_String = Signature_Base_String + "&";

            //5.append parameter string to the output string.
            Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("oauth_consumer_key=" + Properties.Settings.Default.ConsumerKey);
            Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_nonce=" + nonce);
            Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_signature_method=" + "HMAC-SHA1");
            Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_timestamp=" + timeStamp);
            Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_token=" + Properties.Settings.Default.TokenValue);
            Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_version=" + "1.0");

            return Signature_Base_String;
        }

        private static readonly string[] UriRfc3986CharsToEscape = new[] { "!", "*", "'", "(", ")" };

        public static string EscapeUriDataStringRfc3986(string value)
        {
            StringBuilder escaped = new StringBuilder(Uri.EscapeDataString(value));


            for (int i = 0; i < UriRfc3986CharsToEscape.Length; i++)
            {

                escaped.Replace(UriRfc3986CharsToEscape[i], Uri.HexEscape(UriRfc3986CharsToEscape[i][0]));

            }

            // Return the fully-RFC3986-escaped string.

            return escaped.ToString();

        }

        static void Log(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
            Console.WriteLine(message);
        }
    }
}
